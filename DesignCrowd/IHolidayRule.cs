﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignCrowd
{
    public interface IHolidayRule
    {
        IList<DateTime> getHolidays(DateTime firstDate, DateTime secondDate);
    }
}
