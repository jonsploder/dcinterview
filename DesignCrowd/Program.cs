using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignCrowd
{
    public class Program
    {
        public static void Main(string[] args)
        {
        }
    }

    public class PeriodicHoliday : IHolidayRule
    {
        public int month_m;
        public int day_m;
        public PeriodicHoliday(int month, int day)
        {
            month_m = month;
            day_m = day;
        }

        public IList<DateTime> getHolidays(DateTime firstDate, DateTime secondDate)
        {
            List<DateTime> returnList = new List<DateTime>();
            if (firstDate > secondDate)
            {
                return returnList;
            }

            int firstYear = firstDate.Year;
            int secondYear = secondDate.Year;
            int yearsDifference = secondYear - firstYear;
            int counter = 0;
            while (yearsDifference > 0)
            {
                returnList.Add(new DateTime(firstYear + counter, month_m, day_m));
                counter += 1;
                yearsDifference -= 1;
            }
            // Now the edge case of when the years are the same
            DateTime startDay = new DateTime(secondYear, firstDate.Month, firstDate.Day);
            DateTime endDay = new DateTime(secondYear, secondDate.Month, secondDate.Day);
            DateTime lastHoliday = new DateTime(secondYear, month_m, day_m);

            if (startDay < lastHoliday && lastHoliday < endDay)
            {
                returnList.Add(lastHoliday);
            }

            return returnList;
        }
    }

    //! To be done; classes implementing IHolidayRule for 
    // Public holidays which are always on the same day, except when that falls on a weekend. e.g. New
    // Year's Day on January 1st every year, unless that is a Saturday or Sunday, in which case the 
    // holiday is the next Monday.
    // - Public holidays on a certain occurrence of a certain day in a month.e.g.Queen's Birthday on the
    // second Monday in June every year

    public class BusinessDayCounter
    {
        public int WeekdaysBetweenTwoDates(DateTime firstDate, DateTime secondDate)
        {
            //Chop off remainder - test for it too
            DateTime firstDay = new DateTime(firstDate.Year, firstDate.Month, firstDate.Day);
            DateTime secondDay = new DateTime(secondDate.Year, secondDate.Month, secondDate.Day);
            firstDay = firstDay.AddDays(1);
            secondDay = secondDay.AddDays(-1);

            int totalDays = (int)(secondDay - firstDay).TotalDays + 1;
            if (totalDays < 1)
            {
                return 0;
            }
            int totalWeeks = totalDays / 7; // implicit truncation
            int remainderDays = totalDays % 7;

            //Sunday == 0, Monday == 1... let's transform it to monday == 0, ..., Sunday == 6.
            int originalDay = (int)firstDay.DayOfWeek - 1;
            if (originalDay == -1)
            {
                originalDay = 6;
            }
            //Let's see if the remainder days crosses over the weekend now
            int dayCount = originalDay + remainderDays - 1;
            int excess = remainderDays;
            if (dayCount == 5) //Discount Saturday
            {
                excess = remainderDays - 1;
            }
            else if (dayCount >= 6) //Discount Saturday and Sunday
            {
                excess = remainderDays - 2;
            }
            if (excess < 0)
            {
                excess = 0;
            }
            return totalWeeks * 5 + excess;
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<DateTime> publicHolidays)
        {
            // Assumptions: that a public holiday is a whole 24 hour standard day period
            // (no weird things like 8 hours, or 30 hours...) starting from midnight.
            int totalWeekdays = WeekdaysBetweenTwoDates(firstDate, secondDate);
            if (totalWeekdays == 0)
            {
                return 0;
            }
            publicHolidays = publicHolidays.Distinct().ToList();

            DateTime firstDay = new DateTime(firstDate.Year, firstDate.Month, firstDate.Day);
            DateTime secondDay = new DateTime(secondDate.Year, secondDate.Month, secondDate.Day);
            int totalDays = totalWeekdays;
            foreach (DateTime holiday in publicHolidays)
            {
                if (holiday.DayOfWeek != DayOfWeek.Saturday && holiday.DayOfWeek != DayOfWeek.Sunday)
                {
                    // Subtract them from the count if in the interval
                    DateTime holidayDay = new DateTime(holiday.Year, holiday.Month, holiday.Day);
                    if (holidayDay > firstDay && holidayDay < secondDay)
                    {
                        totalDays -= 1;
                    }
                }
            }

            return totalDays;
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<IHolidayRule> holidayRules)
        {
            // We need to account for rules which include the same holidays multiple times, hence a hashSet to remove duplicates
            HashSet<DateTime> aggregatedHolidays = new HashSet<DateTime>();
            List<DateTime> holidays = new List<DateTime>();
            foreach (IHolidayRule rule in holidayRules)
            {
                holidays.AddRange(rule.getHolidays(firstDate, secondDate));
            }

            foreach (DateTime holiday in holidays)
            {
                DateTime sanitizedHoliday = new DateTime(holiday.Year, holiday.Month, holiday.Day);
                aggregatedHolidays.Add(sanitizedHoliday);
            }

            return BusinessDaysBetweenTwoDates(firstDate, secondDate, new List<DateTime>(aggregatedHolidays));
        }
    }
}