﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignCrowd;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class TaskOne
    {
        BusinessDayCounter businessDayCounter = new BusinessDayCounter();
        [TestMethod]
        public void SingleDayInterval()
        {
            DateTime date0 = new DateTime(2013, 10, 7);
            DateTime date1 = new DateTime(2013, 10, 9);
            int result = businessDayCounter.WeekdaysBetweenTwoDates(date0, date1);
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void WeekendsNotCounted()
        {
            DateTime date0 = new DateTime(2013, 10, 5);
            DateTime date1 = new DateTime(2013, 10, 14);
            int result = businessDayCounter.WeekdaysBetweenTwoDates(date0, date1);
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void YearlyTickover()
        {
            DateTime date0 = new DateTime(2013, 10, 7);
            DateTime date1 = new DateTime(2014, 1, 1);
            int result = businessDayCounter.WeekdaysBetweenTwoDates(date0, date1);
            Assert.AreEqual(61, result);
        }

        [TestMethod]
        public void SecondDateEarlier()
        {
            DateTime date0 = new DateTime(2013, 10, 7);
            DateTime date1 = new DateTime(2013, 10, 5);
            int result = businessDayCounter.WeekdaysBetweenTwoDates(date0, date1);
            Assert.AreEqual(0, result);
        }


        [TestMethod]
        public void NoDayInterval()
        {
            DateTime date0 = new DateTime(2013, 10, 7);
            DateTime date1 = new DateTime(2013, 10, 8);
            int result = businessDayCounter.WeekdaysBetweenTwoDates(date0, date1);
            Assert.AreEqual(0, result);
        }
    }

    [TestClass]
    public class TaskTwo
    {
        BusinessDayCounter businessDayCounter = new BusinessDayCounter();
        public List<DateTime> publicHolidays = new List<DateTime>()
                                            {
                                                new DateTime(2013, 12, 25),
                                                new DateTime(2013, 12, 26),
                                                new DateTime(2014, 1, 1)
                                            };
        [TestMethod]
        public void NoBusinessDayInterval()
        {
            DateTime date0 = new DateTime(2013, 10, 7);
            DateTime date1 = new DateTime(2013, 10, 9);
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, publicHolidays);
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void ChristmasBoxingDay()
        {
            DateTime date0 = new DateTime(2013, 12, 24);
            DateTime date1 = new DateTime(2013, 12, 27);
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, publicHolidays);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void YearlyTickoverWithHolidays()
        {
            DateTime date0 = new DateTime(2013, 10, 7);
            DateTime date1 = new DateTime(2014, 1, 1);
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, publicHolidays);
            Assert.AreEqual(59, result);
        }

        [TestMethod]
        public void DuplicateHolidays()
        {
            DateTime date0 = new DateTime(2013, 10, 7);
            DateTime date1 = new DateTime(2014, 1, 1);
            List<DateTime> duplicateHolidays = new List<DateTime>()
            {
                new DateTime(2013, 12, 25),
                new DateTime(2013, 12, 26),
                new DateTime(2014, 1, 1),
                new DateTime(2013, 12, 25),
                new DateTime(2013, 12, 26),
                new DateTime(2014, 1, 1)
            };
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, duplicateHolidays);
            Assert.AreEqual(59, result);
        }

        [TestMethod]
        public void HolidaysOnWeekend()
        {
            DateTime date0 = new DateTime(2015, 12, 25);
            DateTime date1 = new DateTime(2015, 12, 27);
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, publicHolidays);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void HolidaysOnWeekendAndWeekday()
        {
            DateTime date0 = new DateTime(2015, 12, 24);
            DateTime date1 = new DateTime(2015, 12, 27);
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, new List<DateTime>()
                                            {
                                                new DateTime(2015, 12, 25),
                                                new DateTime(2015, 12, 26)
                                            });
            Assert.AreEqual(0, result);
        }
    }

    [TestClass]
    public class TaskThree
    {
        public PeriodicHoliday AnzacDayHoliday = new PeriodicHoliday(4, 25);
        BusinessDayCounter businessDayCounter = new BusinessDayCounter();

        [TestMethod]
        public void AnzacDay()
        {
            List<IHolidayRule> rules = new List<IHolidayRule>()
            {
                AnzacDayHoliday
            };

            DateTime date0 = new DateTime(2015, 4, 24); // Anzac day falls on a Saturday here
            DateTime date1 = new DateTime(2015, 4, 26);
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, rules);
            Assert.AreEqual(0, result);

            date0 = new DateTime(2014, 4, 24); // Anzac day falls on a Friday here
            date1 = new DateTime(2014, 4, 26);
            result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, rules);
            Assert.AreEqual(0, result);

            date0 = new DateTime(2014, 4, 23); // Anzac day falls on a Friday here
            date1 = new DateTime(2014, 4, 26);
            result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, rules);
            Assert.AreEqual(1, result);
        }


        [TestMethod]
        public void DuplicateRules()
        {
            List<IHolidayRule> duplicateRules = new List<IHolidayRule>()
            {
                new PeriodicHoliday(4, 25),
                new PeriodicHoliday(4, 25)
            };

            DateTime date0 = new DateTime(2014, 4, 23); // Anzac day falls on a Friday here
            DateTime date1 = new DateTime(2014, 4, 26);
            int result = businessDayCounter.BusinessDaysBetweenTwoDates(date0, date1, duplicateRules);
            Assert.AreEqual(1, result);
        }
    }
}
